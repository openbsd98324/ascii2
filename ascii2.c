#include <stdio.h>

int main()
{
  for(int c=0x2500; c<0x2600; ++c)
  {
    putchar(0xE0+(c>>12));  putchar(0x80|(c>>6)&0x3F);  putchar(0x80|c&0x3F);
    putchar(' ');  if (c%16==15)  putchar('\n');
  }
  return 0;
}